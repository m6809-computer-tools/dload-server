/*	makebin--convert .s19 file to Color Computer binary format 					*/
/*	1997 Bryan Clingman															*/
/*	bac@realtimeweb.com															*/
/*	This code is in the public domain.											*/

#include <stdio.h>
#include <stdlib.h>

unsigned char	dehex(char *p)
{
	unsigned char a;
	a = (*p < 'A' ? *p - '0' : *p - 'A' + 10) << 4;
	a |= *(p+1) < 'A' ? *(p+1) - '0' : *(p+1) - 'A' + 10;
	return a;
}

struct	stChunk
	{
		unsigned char	*pData;
		unsigned short	sLen;
		unsigned short	sOffset;
		struct stChunk	*pNext;
	};

void insert(struct stChunk *pNew, struct stChunk **pList)
{
	struct stChunk	*pCur;
	struct stChunk	*pLast = 0;
	pCur = *pList;

	while(pCur)
	{
		if (pCur->sOffset > pNew->sOffset)
		{
			if (pLast)
			{
				pLast->pNext = pNew;
				pNew->pNext = pCur;
			}
			else
			{
				*pList = pNew;
				pNew->pNext = pCur;
			}
			return;
		}
		pLast = pCur;
		pCur = pCur->pNext;
	}
	if (*pList)
		pLast->pNext = pNew;
	else
		*pList = pNew;

	pNew->pNext = 0;
}

int main(int	argc, char **argv)
{
	FILE	*fpi, *fpo;
	unsigned char	caLine[100];
	short	sLineCount,
			sII;
	int		iLen;
	struct	stChunk	*pList = 0;
	struct	stChunk	*pCur;

	if (argc < 2)
	{
		fprintf (stderr, "No input file\n");
		exit(1);
	}
		
	fpi = fopen(argv[1], "r");
	if (!fpi)
	{
		fprintf (stderr, "No input file %s\n", argv[1]);
		exit(1);
	}

	if (argc > 2)
	{
		fpo = fopen(argv[2], "w");
		if (!fpo)
		{
			fprintf (stderr, "No unable to create output file %s\n", argv[2]);
			exit(1);
		}
	}
	else
		fpo = stdout;
	sLineCount = 0;
	while(fgets(caLine, 100, fpi))
	{
		sLineCount++;
		if (caLine[0] != 'S')
			goto error;
		switch (caLine[1])
		{
			case '1':
				pCur = (struct stChunk *)malloc(sizeof (struct stChunk));
				pCur->sLen = dehex(&caLine[2]) - 3;
				pCur->sOffset = dehex(&caLine[4]) << 8 | dehex(&caLine[6]);
				pCur->pData = (unsigned char *)malloc(pCur->sLen);
				for (sII = 0; sII < pCur->sLen; sII++)
					pCur->pData[sII] = dehex(&caLine[(sII << 1) + 8]);
				pCur->pNext = 0;
				insert(pCur, &pList);
				break;
			case '9':
				break;
			default:
				goto error;
		}
	}
	pCur = pList;
	while (pCur->pNext)
	{
		if (pCur->sOffset + pCur->sLen != pCur->pNext->sOffset)
			fprintf(stderr, "discontinuous at %x\n", pCur->sOffset);
		pCur = pCur->pNext;
	}

	iLen = pCur->sOffset + pCur->sLen - pList->sOffset;
	fprintf(stderr, "Total Length: %d\n", iLen);
	// the output:
	caLine[0] = 0;
	caLine[1] = (unsigned char)((iLen & 0xff00) >> 8);
	caLine[2] = (unsigned char)(iLen & 0xff);
	caLine[3] = (unsigned char)((pList->sOffset & 0xff00) >> 8);
	caLine[4] = (unsigned char)(pList->sOffset & 0xff);
	fwrite(caLine , 1, 5, fpo);
	pCur = pList;
	while (pCur->pNext)
	{
		fwrite(pCur->pData, 1, pCur->sLen, fpo);
		for (sII = pCur->sOffset + pCur->sLen; sII < pCur->pNext->sOffset; sII++)
			fwrite(caLine, 1, 1, fpo);
		pCur = pCur->pNext;
	}
	fwrite(pCur->pData, 1, pCur->sLen, fpo);

	caLine[0] = 0xff;
	caLine[1] = 0;
	caLine[2] = 0;
	caLine[3] = (unsigned char)((pList->sOffset & 0xff00) >> 8);
	caLine[4] = (unsigned char)(pList->sOffset & 0xff);
	fwrite(caLine , 1, 5, fpo);
	fclose(fpo);

	return 0;
	error:
		printf("Error in line %d\n", sLineCount);
		fclose(fpi);
		fclose(fpo);
		return 1;
}
